package src;

import java.util.Scanner;

import src.Account;
import src.Login;
import src.SaveData;
import src.LoadData;
import src.Player;
import src.Character;

public class Driver {
	
	static Login log= new Login();
	static LoadData loader= new LoadData();
	static Player player= new Player();
	static SaveData saver= new SaveData();
	
	public static void main(String args[]){
		
		log.intro();
		
		if(log.returnType()==0){
			
			Account x= new Account(log.returnUname(),log.returnPword());
			player.setStats();
			saver.save(x.username, x.password, player.intellegence, player.charisma, 
					player.looks, player.strength, player.luck);
			
		}else{
			
			Account z= new Account(loader.getUname(), loader.getPword());
		}
		
		Character Sally= new Character(50,50,75,1,0);
		Character Phill= new Character(25,50,60,0,1);
		Character Andrew= new Character(50,50,50,0,0);
	}
}
