package src;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
 
public class SaveData {
	
	static String uname;
	static String pword;
	static int intel, charism, look, stren, luc;
	
	//Saves Document to an xml file
	// AGAIN, MUST BE CHANGED TO A KNOWN DIRECTORY WITH READ/WRITE PERMISSIONS OR WILL
	// NOT WORK
	public static void save(String x, String y, int a, int b, int c, int d, int e){
		
		uname=x;
		pword=y;
		intel=a;
		charism=b;
		look=c;
		stren=d;
		luc=e;
	
		  try {
	 
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Game");
			doc.appendChild(rootElement);
	 
			// Account elements
			Element account = doc.createElement("Account");
			rootElement.appendChild(account);
	 
			// set attribute to staff element
			Attr attr = doc.createAttribute("id");
			attr.setValue("1");
			account.setAttributeNode(attr);
	 
			// Username elements
			Element firstname = doc.createElement("Username");
			firstname.appendChild(doc.createTextNode(uname));
			account.appendChild(firstname);
	 
			// Password elements
			Element password= doc.createElement("Password");
			password.appendChild(doc.createTextNode(pword));
			account.appendChild(password);
	 
			// Intellegence elements
			Element intellegence = doc.createElement("Intellegence");
			intellegence.appendChild(doc.createTextNode(Integer.toString(intel)));
			account.appendChild(intellegence);
			
			// Charisma elements
			Element charisma = doc.createElement("Charisma");
			charisma.appendChild(doc.createTextNode(Integer.toString(charism)));
			account.appendChild(charisma);
						
			// Looks elements
			Element looks = doc.createElement("Looks");
			looks.appendChild(doc.createTextNode(Integer.toString(look)));
			account.appendChild(looks);
			
			
			// Strength elements
			Element strength = doc.createElement("Strength");
			strength.appendChild(doc.createTextNode(Integer.toString(stren)));
			account.appendChild(strength);
			
			// Luck elements
			Element luck= doc.createElement("Luck");
			luck.appendChild(doc.createTextNode(Integer.toString(luc)));
			account.appendChild(luck);
						
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			
			//Must be changed to a known directory with permissions, in my case, E:\\
			StreamResult result = new StreamResult(new File("E:\\file.xml"));
	 
			transformer.transform(source, result);
	 
			System.out.println("File saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		}
}

