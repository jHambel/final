package src;

import java.util.Scanner;

public class Player {
	
	Scanner scan= new Scanner(System.in);
	
	//Initial Stats
	int intellegence=0;
	int charisma=0;
	int looks=0;
	int strength=0;
	int luck=0;
	
	//0 is male, 1 is female
	int sex=0;
	
	//Max Points
	int points=25;
	
	//Output stats to console
	public void showStats(){
		
		System.out.println("Current stats: \n " +
			 	   		   "Intellegence: "+ intellegence + "\n" +
			 	   		   "Charisma: " + charisma + "\n" +
			 	   		   "Looks: " + looks + "\n" +
			 	   		   "Strength: " + strength + "\n" +
			 	   		   "luck: " + luck);
		System.out.println("You currently have " + points + " stat points to spend");
	}
	
	
	//Add points to a stat
	public void addStat(){
		System.out.println("Which stat would you like to add to?");
		
		String z= scan.next();
		
		if(z.equals("intellegence")){
		
			System.out.println("Remaining stat points: " + points);
			System.out.println("How many points would you like to add to intellegence?");
			int x=scan.nextInt();
			if(x>points){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot add a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Adding " + x + " points to intellegence.");
				points-=x;
				intellegence+=x;
				setStats();
			}
			
		}else if(z.equals("strength")){
			
			System.out.println("Remaining stat points: " + points);
			System.out.println("How many points would you like to add to strength?");
			int x=scan.nextInt();
			if(x>points){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot add a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Adding " + x + " points to strength");
				points-=x;
				strength+=x;
				setStats();
			}
			
		}else if(z.equals("looks")){
			
			System.out.println("Remaining stat points: " + points);
			System.out.println("How many points would you like to add to looks?");
			int x=scan.nextInt();
			if(x>points){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot add a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Adding " + x + " points to looks.");
				points-=x;
				looks+=x;
				setStats();
			}
			
		}else if(z.equals("charisma")){
			
			System.out.println("Remaining stat points: " + points);
			System.out.println("How many points would you like to add to charisma?");
			int x=scan.nextInt();
			if(x>points){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot add a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Adding " + x + " points to charisma.");
				points-=x;
				charisma+=x;
				setStats();
			}
			
		}else if(z.equals("luck")){
			
			System.out.println("Remaining stat points: " + points);
			System.out.println("How many points would you like to add to luck?");
			int x=scan.nextInt();
			if(x>points){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot add a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Adding " + x + " points to luck.");
				points-=x;
				luck+=x;
				setStats();
			}
			
		}else{
			System.out.println("Thats not an option. Please enter a valid stat.");
			setStats();
		}
	}
	
	
	//Subtract points from a stat
	public void subStat(){
		System.out.println("Which stat would you like to subtract from?");
		
		String z= scan.next();
		
		if(z.equals("intellegence")){
		
			System.out.println("Points in intellegence " + intellegence);
			System.out.println("How many points would you like to subtract from "
							 + "intellegence?");
			int x=scan.nextInt();
			if(x>intellegence){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot subtract a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Subtracting " + x + " points from intellegence.");
				points+=x;
				intellegence-=x;
				setStats();
			}
			
		}else if(z.equals("strength")){
			
			System.out.println("Points in strength " + strength);
			System.out.println("How many points would you like to subtract from "
							 + "strength?");
			int x=scan.nextInt();
			if(x>strength){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot subtract a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Subtracting " + x + " points from strength.");
				points+=x;
				strength-=x;
				setStats();
			}
			
		}else if(z.equals("looks")){
			
			System.out.println("Points in looks " + looks);
			System.out.println("How many points would you like to subtract from "
							 + "looks?");
			int x=scan.nextInt();
			if(x>looks){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot subtract a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Subtracting " + x + " points from looks.");
				points+=x;
				looks-=x;
				setStats();
			}
			
		}else if(z.equals("charisma")){
			
			System.out.println("Points in charisma " + charisma);
			System.out.println("How many points would you like to subtract from "
							 + "charisma?");
			int x=scan.nextInt();
			if(x>charisma){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot subtract a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Subtracting " + x + " points from charisma.");
				points+=x;
				charisma-=x;
				setStats();
			}
			
		}else if(z.equals("luck")){
			
			System.out.println("Points in luck " + luck);
			System.out.println("How many points would you like to subtract from "
							 + "luck?");
			int x=scan.nextInt();
			if(x>luck){
				
				System.out.println("You dont have enough points to do that. Try again.");
				setStats();
			}else if(x<0){
				
				System.out.println("You cannot subtract a negative number. Try again.");
				setStats();
			}else{
				System.out.println("Subtracting " + x + " points from luck.");
				points+=x;
				luck-=x;
				setStats();
			}
			
		}else{
			System.out.println("Thats not an option. Please enter a valid stat.");
			setStats();
		}
	}
	
	//The actual method to change the stats
	public void setStats(){
		
		showStats();
		System.out.println("Would you like to ADD to a stat, SUBtract from a stat, or "
						 + "CONTinue with the game?");
		String x= scan.next();
		
		if(x.equals("add")){
			
			addStat();
			setStats();
			
		}else if(x.equals("sub")){
			
			subStat();
			setStats();
		}else if(x.equals("cont")){
			
			System.out.println("Saving Data to E:\\file.xml");
			
		}else{
			
			System.out.println("That is not an option choice!");
			setStats();
		}
	}
}
