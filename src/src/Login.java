package src;

import java.util.Scanner;

import src.Account;
import src.LoadData;

//Login Class to login to old accounts 
public class Login {
	
	Scanner scan= new Scanner(System.in);
	LoadData loader = new LoadData();
	int type;
	String uname;
	String pword;
	
	//Intro to the program, determines new players and creates the xml file if it doesnt exist
	public void intro(){
		
		System.out.println("Welcome to Murder Mystery. Are you a (N)ew player "
				+ "or a (R)eturning player");
		
		String n= scan.next();
	
		if(n.equals("n")){
			
			System.out.println("Please create an account.");
			System.out.println("What will your username be: ");
			uname= scan.next();
			System.out.println("Please enter a password: ");
			pword= scan.next();
			
			type=0;
			
		}else if(n.equals("r")){
			
			type=1;
			loader.load();
			System.out.println("Please enter your password to proceed");
			
			if(scan.next().equals(loader.getPword())){
				
				System.out.println("Welcome back!");
				
				
			}else{
				
				System.out.println("Wrong Password. Restarting game......");
				intro();
			}
		}else{
			
			System.out.println("Thats not an option, please try again!");
			intro();
		}
	}
	
	public String returnUname(){
		
		return uname;
	}
	
	public String returnPword(){
		
		return pword;
	}
	public int returnType(){
		
		return type;
	}
}
