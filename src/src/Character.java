package src;

public class Character {

		//These levels are the initial stats of the characters. 
		//50 is mid, 0 is min, 100 is max
		int stress=50;
		int trust=50;
		int attraction=50;
		
		//0 is male, 1 is female
		int sex=0;
		
		//1 if it is the murderer, 0 if not
		int isMurderer=0;
		
		public Character(int s, int t, int a, int se, int iM){
			
			stress=s;
			trust= t;
			attraction=a;
			sex=se;
			isMurderer=iM;
		}
		public void setStress(int x){
			
			stress=x;
		}
		
		public void setTrust(int x){
			
			trust = x;
		}
		
		public void setAttraction(int x){
			
			attraction=x;
		}
		
		public void setSex(int x){
			
			sex=x;
		}
		
		public void setMurder(int x){
			
			isMurderer=x;
		}
}
