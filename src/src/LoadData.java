package src;

import java.io.File;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;


//Loads Data from target location(in this case E:\\)

//                                    NOTE PROFESSOR:
// When testing this, change "E:\\file.xml" to a directory in which you have read/write 
// permissions, otherwise it WILL NOT WORK

public class LoadData {
	
	static String uname;
	static String pword;
	static int intel, charism, look, stren, luc;
	
	//Loads data from the xml file and puts them into the appropiate data type
	public static void load(){
		
	    try {
	 
	    	File file = new File("E:\\file.xml");
	    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder builder = factory.newDocumentBuilder();
	    	Document doc = builder.parse(file);
	    	doc.getDocumentElement().normalize();
	 
	    	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	 
	    	NodeList nList = doc.getElementsByTagName("Account");
	 
	    	System.out.println("----------------------------");
	 
	    	for (int temp = 0; temp < nList.getLength(); temp++) {
	 
	    		Node nNode = nList.item(temp);
	 
	    		System.out.println("\nCurrent Element :" + nNode.getNodeName());
	 
	    		if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	 
	    			Element eElement = (Element) nNode;
	    			
	    			System.out.println("Information found!");
	    			System.out.println("Account id : " + eElement.getAttribute("id"));
				
	    			System.out.println("Username : " + eElement.getElementsByTagName("Username").item(0).getTextContent());
	    			uname=eElement.getElementsByTagName("Username").item(0).getTextContent();
				
	    			//System.out.println("Password : " + eElement.getElementsByTagName("Password").item(0).getTextContent());
	    			pword=eElement.getElementsByTagName("Password").item(0).getTextContent();
				
	    			System.out.println("Intellegence : " + eElement.getElementsByTagName("Intellegence").item(0).getTextContent());
	    			intel=Integer.parseInt(eElement.getElementsByTagName("Intellegence").item(0).getTextContent());
				
	    			System.out.println("Charisma : " + eElement.getElementsByTagName("Charisma").item(0).getTextContent());
	    			charism=Integer.parseInt(eElement.getElementsByTagName("Charisma").item(0).getTextContent());
				
	    			System.out.println("Looks : " + eElement.getElementsByTagName("Looks").item(0).getTextContent());
	    			look=Integer.parseInt(eElement.getElementsByTagName("Looks").item(0).getTextContent());
				
	    			System.out.println("Strength : " + eElement.getElementsByTagName("Strength").item(0).getTextContent());
	    			stren=Integer.parseInt(eElement.getElementsByTagName("Strength").item(0).getTextContent());
				
	    			System.out.println("Luck : " + eElement.getElementsByTagName("Luck").item(0).getTextContent());
	    			luc=Integer.parseInt(eElement.getElementsByTagName("Luck").item(0).getTextContent());
	    		}
	    	}
	    }catch (Exception e) {
	    	e.printStackTrace();
	    }
	  }
	 
	
	  //Normal get statements to return values
	  public String getUname(){
		
		  return uname;
	  }
	  
	  public String getPword(){
		  
		  return pword;
	  }
	  
	  public int getIntel(){
		  
		  return intel;
	  }
	  
	  public int getCharism(){
		  
		  return charism;
	  }
	  
	  public int getLooks(){
		  
		  return look;
	  }
	  
	  public int getStren(){
		  
		  return stren;
	  }
	  
	  public int getLuck(){
		  
		  return luc;
	  }
}